package com.kafka.tutorial.consumer;

import com.kafka.tutorial.model.Customer;
import com.kafka.tutorial.model.CustomerObjectDeSerializer;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.serialization.IntegerDeserializer;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.time.Duration;
import java.util.Arrays;
import java.util.Properties;

public class SimpleKafkaConsumer<K extends Serializable,V extends Serializable> {
    private static final Logger logger = LoggerFactory.getLogger(SimpleKafkaConsumer.class);
    private static final String TOPIC = "topic.customer";
    private static final String BOOTSTRAP_SERVERS = "localhost:9092,localhost:9093";

    private String topic;

    public SimpleKafkaConsumer(String topic) {
        this.topic = topic;
    }

    public Properties getProperties() {
        Properties properties = new Properties();
        properties.setProperty(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, BOOTSTRAP_SERVERS);
        properties.setProperty(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, IntegerDeserializer.class.getName());
        properties.setProperty(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, CustomerObjectDeSerializer.class.getName());
        properties.setProperty(ConsumerConfig.GROUP_ID_CONFIG, "consumer-group-1");
        properties.setProperty(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");

        return properties;
    }

    public KafkaConsumer<K,V> getConsumer() {
        KafkaConsumer<K, V> kafkaConsumer = new KafkaConsumer<>(getProperties());
        kafkaConsumer.subscribe(Arrays.asList(TOPIC));
        return kafkaConsumer;
    }

    public static void main(String [] args) {
        SimpleKafkaConsumer simpleKafkaConsumer = new SimpleKafkaConsumer(TOPIC);
        KafkaConsumer consumer = simpleKafkaConsumer.getConsumer();
        while(true) {
            Iterable<ConsumerRecord<Integer, Customer>> records =
                    consumer.poll(Duration.ofMillis(100)).records(simpleKafkaConsumer.topic);

            records.forEach(record -> {
                logger.info("key-{}, value-{}", record.key(), record.value());
            });

        }

    }



}
