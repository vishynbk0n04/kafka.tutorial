package com.kafka.tutorial.producer;

import com.kafka.tutorial.consumer.SimpleKafkaConsumer;
import com.kafka.tutorial.model.Customer;
import com.kafka.tutorial.model.CustomerObjectDeSerializer;
import com.kafka.tutorial.model.CustomerObjectSerializer;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.IntegerDeserializer;
import org.apache.kafka.common.serialization.IntegerSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Random;
import java.util.stream.IntStream;

public class SimpleKafkaProducer {
    private static final Logger logger = LoggerFactory.getLogger(SimpleKafkaProducer.class);
    private static final String TOPIC = "topic.customer";
    private static final String BOOTSTRAP_SERVERS = "localhost:9092,localhost:9093";
    private List<ProducerRecord> data = new ArrayList();

    private String topic;

    public SimpleKafkaProducer(String topic) {
        this.topic = topic;
    }

    public Properties setupProperties() {
        Properties properties = new Properties();
        properties.setProperty(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, BOOTSTRAP_SERVERS);
        properties.setProperty(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, IntegerSerializer.class.getName());
        properties.setProperty(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, CustomerObjectSerializer.class.getName());
        properties.setProperty(ProducerConfig.CLIENT_ID_CONFIG, "SimpleKafkaProducer");

        return properties;
    }
    public void writeMessages(Properties properties) {
        IntStream.range(101, 200).forEach(number -> {
            Customer customer = new Customer();
            customer.setAge(number);
            customer.setCustomerId(new Random().nextInt());
            customer.setAge(30);
            customer.setName("Customer-" + number);
            data.add(new ProducerRecord(topic, number, customer));
        });
        KafkaProducer producer = new KafkaProducer(properties);
        data.stream().forEach(producerRecord -> {
            producer.send(producerRecord);
        });
        producer.flush();
        producer.close();

    }

    public static void main(String [] args) {
        SimpleKafkaProducer simpleKafkaProducer = new SimpleKafkaProducer(TOPIC);
        Properties properties = simpleKafkaProducer.setupProperties();
        simpleKafkaProducer.writeMessages(properties);
    }
}
