package com.kafka.tutorial.model;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.kafka.common.serialization.Deserializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class CustomerObjectDeSerializer implements Deserializer<Customer> {
    private static final Logger logger = LoggerFactory.getLogger(CustomerObjectDeSerializer.class);

    @Override
    public Customer deserialize(String s, byte[] bytes) {
        Customer customer = new Customer();
        try {
            customer = new ObjectMapper().readValue(bytes, Customer.class);
        } catch (Exception exception) {
            logger.error("Exception while de-serializing customer object");
        }
        return customer;
    }
}
