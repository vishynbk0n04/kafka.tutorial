package com.kafka.tutorial.model;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.kafka.common.serialization.Serializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CustomerObjectSerializer implements Serializer<Customer> {

    private static final Logger logger = LoggerFactory.getLogger(CustomerObjectSerializer.class);

    @Override
    public byte[] serialize(String s, Customer customer) {
        byte[] retVal = null;
        try {
            retVal = new ObjectMapper().writeValueAsBytes(customer);
        } catch (JsonProcessingException exception) {
            logger.error("There was an error parsing customer object");
        }
        return retVal;
    }
}
