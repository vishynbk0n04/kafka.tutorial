package com.kafka.tutorial.model;

import lombok.*;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Customer implements Serializable {
    private String name;
    private Integer customerId;
    private int age;
    private String occupation;
}
